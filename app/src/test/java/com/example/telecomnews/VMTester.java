package com.example.telecomnews;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.telecomnews.model.EventMessage;
import com.example.telecomnews.model.EventType;
import com.example.telecomnews.ui.MainActivity;
import com.example.telecomnews.util.Repository;
import com.example.telecomnews.vm.NewsViewModel;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;

import static org.junit.Assert.*;

@Config(manifest = Config.NONE)
@RunWith(RobolectricTestRunner.class)
public class VMTester {

    @Mock
    MainActivity mainActivity;

    NewsViewModel viewModel;

    @Before
    public void setup(){
        mainActivity = Robolectric.buildActivity(MainActivity.class).get();
        App.setContext(mainActivity.getApplication());
        Repository.getInstance().testing = true;
        viewModel = ViewModelProviders.of(mainActivity).get(NewsViewModel.class);
    }

    @After
    public void tearDown(){
        Repository.getInstance().testing = false;
    }

    @Test
    public void checkVMMethods(){
        try {
            viewModel.refreshFeed();
            viewModel.apiDone();
            viewModel.checkForPagination(0);
            viewModel.paginate(0);
        } catch (Exception e){
            System.out.println(e);
            assertTrue(e == null);
        }
    }

    @Test
    public void checkRepoComm(){

        ArrayList<EventType> types = new ArrayList<>();

        new Thread(new Runnable() {
            @Override
            public void run() {
                viewModel.eventMessageLiveData.observeForever(new Observer<EventMessage>() {
                    @Override
                    public void onChanged(EventMessage eventMessage) {
                        if(types.size() < 2) {
                            types.add(eventMessage.getType());
                            return;
                        }
                        types.add(eventMessage.getType());
                        assertTrue(types.get(0) == EventType.API_LOADING);
                        assertTrue(types.get(1) == EventType.API_OVER);
                        assertTrue(types.get(2) == EventType.NEWS_API_FAILURE);
                    }
                });
            }
        }).run();

        Repository.getInstance().exposeCommunicator().postValue(new EventMessage(EventType.API_LOADING));
        Repository.getInstance().exposeCommunicator().postValue(new EventMessage(EventType.API_OVER));
        Repository.getInstance().exposeCommunicator().postValue(new EventMessage(EventType.NEWS_API_FAILURE));
    }

    @Test
    public void checkSynchronisation(){

        final int[] k = {0};

        new Thread(new Runnable() {
            @Override
            public void run() {
                viewModel.eventMessageLiveData.observeForever(new Observer<EventMessage>() {
                    @Override
                    public void onChanged(EventMessage eventMessage) {
                        assertTrue(k[0]++ == 0);
                    }
                });
            }
        }).run();
        for (int i = 0; i < 50; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Repository.getInstance().getNews();
                }
            }).start();

        }
    }

}
