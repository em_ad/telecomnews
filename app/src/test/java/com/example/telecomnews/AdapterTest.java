package com.example.telecomnews;

import androidx.paging.DataSource;
import androidx.paging.PagedList;

import com.example.telecomnews.callback.ItemClickListener;
import com.example.telecomnews.model.NewsItem;
import com.example.telecomnews.ui.adapter.NewsAdapter;
import com.example.telecomnews.util.DBaccess;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.concurrent.Executor;

import static org.junit.Assert.*;

@Config(manifest = Config.NONE)
@RunWith(RobolectricTestRunner.class)
public class AdapterTest {

    @Mock
    NewsAdapter adapter;

    @Mock
    PagedList<NewsItem> paged;


    @Before
    public void setup() {
        adapter = new NewsAdapter(new ItemClickListener() {
            @Override
            public void itemClicked(int position) {
                System.out.println("item clicked");
            }
        });
    }

    @After
    public void tearDown() {
        adapter = null;
    }

    @Test
    public void checkSizeIncrementation() {
        assertTrue(adapter.getItemCount() == 0);
        final PagedList.Config myConfig = new PagedList.Config.Builder()
                .setInitialLoadSizeHint(50)
                .setPageSize(50)
                .build();

        DataSource<Integer, NewsItem> integerContactDataSource = DBaccess.getInstance().getDb().newsDao().getAll().create();
        paged = new PagedList.Builder<>(integerContactDataSource, myConfig)
                .setInitialKey(0).setNotifyExecutor(new Executor() {
                    @Override
                    public void execute(Runnable runnable) {
                        new Thread(runnable).start();
                    }
                }).setFetchExecutor(new Executor() {
                    @Override
                    public void execute(Runnable runnable) {
                        new Thread(runnable).start();
                    }
                })
                .build();

        adapter.submitList(paged);
        assertTrue(adapter.getItemCount() == DBaccess.getInstance().getDb().newsDao().getAllSimple().size());
    }

}
