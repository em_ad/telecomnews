package com.example.telecomnews;

import android.app.Application;

import com.example.telecomnews.ui.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;

@Config(manifest = Config.NONE)
@RunWith(RobolectricTestRunner.class)
public class ApplicationTest {

    @Mock
    MainActivity activity;

    @Before
    public void setup(){
        activity = Robolectric.buildActivity(MainActivity.class).get();
    }

    @Test
    public void checkAppContext(){
        Application context = App.getContext();
        App.setContext(activity.getApplication());
        assertEquals(App.getContext(), context ); //checking app is activity's application
    }

}
