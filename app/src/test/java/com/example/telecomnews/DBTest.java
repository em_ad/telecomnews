package com.example.telecomnews;

import android.os.AsyncTask;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.paging.DataSource;
import androidx.room.Room;

import com.example.telecomnews.callback.DBinterface;
import com.example.telecomnews.callback.NewsDao;
import com.example.telecomnews.model.NewsItem;
import com.example.telecomnews.util.Constants;
import com.example.telecomnews.ui.MainActivity;
import com.example.telecomnews.util.DBaccess;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Config(manifest = Config.NONE)
@RunWith(RobolectricTestRunner.class)
public class DBTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    final static int DB_SIZE = 5;

    @Mock
    MainActivity activity = null;

    DBinterface dbInterface = null;

    NewsDao newsdao = null;

    List<NewsItem> mockNews = new ArrayList<>();

    Thread thread;

    @Before
    public void setup(){
        activity = Robolectric.buildActivity(MainActivity.class).get();
        App.setContext(activity.getApplication());
        dbInterface = Room.databaseBuilder(activity, DBinterface.class, Constants.ROOM_NAME).build();
        for (int i = 0; i < DB_SIZE; i++) {
            mockNews.add(new NewsItem(i, "title" + i, "des" + i , "DESCRIPTION " + i));
        }
        newsdao = new NewsDao() {
            @Override
            public DataSource.Factory<Integer, NewsItem> getAll() {
                return dbInterface.newsDao().getAll();
            }

            @Override
            public List<NewsItem> getAllSimple() {
                return mockNews;
            }

            @Override
            public void insertAll(List<NewsItem> newsItems) {
                mockNews.addAll(newsItems);
            }

            @Override
            public void insert(NewsItem newsItem) {
                mockNews.add(newsItem);
            }

            @Override
            public NewsItem loadSingle(int id) {
                for (int i = 0; i < mockNews.size(); i++) {
                    if(mockNews.get(i).getId() == id)
                        return mockNews.get(i);
                }
                return null;
            }

            @Override
            public void delete(NewsItem item) {
                mockNews.remove(item);
            }
        };
    }

    @After
    public void tearDown(){

    }

    @Test
    public void testRoomCreation() throws InterruptedException {
        assertFalse(dbInterface.newsDao().getAll().create().isInvalid());
        assertTrue(newsdao.getAllSimple().size() == 5);

    }

    @Test
    public void checkActualDB(){
        System.out.println("thread start");
        NewsItem test = new NewsItem(0, "", "", "");
        int size0 = DBaccess.getInstance().getDb().newsDao().getAllSimple().size();
        DBaccess.getInstance().getDb().newsDao().insert(test);
        int size1 = DBaccess.getInstance().getDb().newsDao().getAllSimple().size();
        assertTrue(size1 == size0 + 1);
        assertTrue(DBaccess.getInstance().getDb().newsDao().loadSingle(0).getTitle().equals(""));
        DBaccess.getInstance().getDb().newsDao().delete(test);
        assertTrue(DBaccess.getInstance().getDb().newsDao().getAllSimple().size() == size0);
        System.out.println("thread finish");
    }


}
