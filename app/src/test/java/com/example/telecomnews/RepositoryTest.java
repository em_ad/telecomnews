package com.example.telecomnews;


import android.app.Application;
import android.os.AsyncTask;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;

import com.example.telecomnews.model.EventMessage;
import com.example.telecomnews.model.EventType;
import com.example.telecomnews.model.NewsItem;
import com.example.telecomnews.model.NewsResponse;
import com.example.telecomnews.util.Repository;
import com.example.telecomnews.ui.MainActivity;
import com.example.telecomnews.vm.NewsViewModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;

import java.util.ArrayList;

import retrofit2.Callback;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.*;

@Config(manifest = Config.NONE)
@RunWith(RobolectricTestRunner.class)
public class RepositoryTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Mock
    MainActivity mainActivity;

    @Mock
    NewsViewModel viewModel;

    @Captor
    private ArgumentCaptor<Callback<NewsResponse>> callbackArgumentCaptor;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        App.setContext((Application) getInstrumentation().getTargetContext().getApplicationContext());
        ActivityController<MainActivity> controller = Robolectric.buildActivity(MainActivity.class);
        mainActivity = controller.get();
        viewModel = ViewModelProviders.of(mainActivity).get(NewsViewModel.class);
    }

    @Test
    public void checkRepo(){

        ArrayList<EventType> events = new ArrayList<>();

        viewModel.eventMessageLiveData.observeForever(new Observer<EventMessage>() {
            @Override
            public void onChanged(EventMessage eventMessage) {
                assertTrue(viewModel.eventMessageLiveData.getValue().getType() == EventType.API_LOADING); //holds state
            }
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                viewModel.liveData.observeForever(new Observer<PagedList<NewsItem>>() {
                    @Override
                    public void onChanged(PagedList<NewsItem> newsItems) {
                        System.out.println("NEWS : " + newsItems.size());
                    }
                });
            }
        }).run();
        Repository.getInstance().getNews();
    }

    @Test
    public void checkLiveData(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Repository.getInstance().exposeCommunicator().observeForever(new Observer<EventMessage>() {
                    @Override
                    public void onChanged(EventMessage eventMessage) {
                        assertTrue(eventMessage.getType() == EventType.API_LOADING);
                    }
                });
            }
        }).run();
        Repository.getInstance().exposeCommunicator().postValue(new EventMessage(EventType.API_LOADING));
    }
}
