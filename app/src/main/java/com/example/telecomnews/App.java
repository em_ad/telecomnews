package com.example.telecomnews;

import android.app.Application;
import android.content.Context;

public class App extends Application {

    private static Application context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    public static Application getContext() {
        return context;
    }

    public static void setContext(Application context) {
        App.context = context;
    }
}
