package com.example.telecomnews.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.telecomnews.R;
import com.example.telecomnews.ui.fragment.MainFragment;
import com.example.telecomnews.util.FragmentUtils;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentUtils.addFragment(MainActivity.this, R.id.flContainer, MainFragment.newInstance() );
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1)
            getSupportFragmentManager().popBackStackImmediate();
        else
            finish();
    }
}
