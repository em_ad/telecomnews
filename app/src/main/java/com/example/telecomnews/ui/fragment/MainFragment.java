package com.example.telecomnews.ui.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.telecomnews.R;
import com.example.telecomnews.callback.ItemClickListener;
import com.example.telecomnews.model.EventMessage;
import com.example.telecomnews.model.NewsItem;
import com.example.telecomnews.ui.MainActivity;
import com.example.telecomnews.ui.adapter.NewsAdapter;
import com.example.telecomnews.util.FragmentUtils;
import com.example.telecomnews.vm.NewsViewModel;

public class MainFragment extends Fragment {

    private NewsViewModel viewModel;
    private NewsAdapter adapter;
    private SwipeRefreshLayout refresher;
    private LinearLayoutManager layoutManager;
    private ProgressBar progress;

    public MainFragment() {
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(NewsViewModel.class);
        setupViewsAndRecycler(view);
        setObserverOnVM();
    }

    private void setupViewsAndRecycler(View view) {
        RecyclerView recycler = view.findViewById(R.id.recycler);
        refresher = view.findViewById(R.id.refresher);
        progress = view.findViewById(R.id.progress);
        adapter = new NewsAdapter(new ItemClickListener() {
            @Override
            public void itemClicked(int position) {
                NewsItem newsItem = adapter.getCurrentList().get(position);
                if (getActivity() != null)
                    FragmentUtils.addFragment((MainActivity) getActivity(), R.id.flContainer, NewsFragment.newInstance(newsItem));
            }
        });
        layoutManager = new LinearLayoutManager((getContext()));
        recycler.setAdapter(adapter);
        recycler.setLayoutManager(layoutManager);
        refresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                viewModel.refreshFeed();
            }
        });
        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0)
                    viewModel.checkForPagination(layoutManager.findLastVisibleItemPosition());
            }
        });
    }

    private void setObserverOnVM() {
        viewModel.liveData.observe(this, new Observer<PagedList<NewsItem>>() {
            @Override
            public void onChanged(PagedList<NewsItem> newsItems) {
                viewModel.checkResults(newsItems, adapter);
            }
        });
        viewModel.eventMessageLiveData.observe(this, new Observer<EventMessage>() {
            @Override
            public void onChanged(EventMessage eventMessage) {
                if (eventMessage.getNewsItem() != null) return;
                switch (eventMessage.getType()) {
                    case API_LOADING:
                        progress.setVisibility(View.VISIBLE);
                        break;
                    case NEWS_API_FAILURE:
                        viewModel.apiDone();
                        dataLoaded();
                        Toast.makeText(getContext(), R.string.api_fail_string, Toast.LENGTH_LONG).show();
                        break;
                    case NEWS_API_DONE:
                        viewModel.apiDone();
                        dataLoaded();
                        break;
                    case API_OVER:
                        dataLoaded();
                        break;

                }
            }
        });

    }


    public void dataLoaded() {
        refresher.setRefreshing(false);
        progress.setVisibility(View.GONE);
    }

    public void dataEmpty() {
        Toast.makeText(getContext(), R.string.api_empty_string, Toast.LENGTH_SHORT).show();
    }


}
