package com.example.telecomnews.ui.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.telecomnews.R;
import com.example.telecomnews.model.EventMessage;
import com.example.telecomnews.model.EventType;
import com.example.telecomnews.model.NewsItem;
import com.example.telecomnews.util.Repository;
import com.example.telecomnews.vm.NewsViewModel;
import com.squareup.picasso.Picasso;


public class NewsFragment extends Fragment implements View.OnClickListener {

    private NewsItem newsItem;
    private NewsViewModel newsViewModel;
    private View view;
    private TextView tvSync;

    public NewsFragment() {
    }

    public static NewsFragment newInstance(NewsItem newsItem) {
        NewsFragment fragment = new NewsFragment();
        fragment.newsItem = newsItem;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_news, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        if(getActivity() != null)
            newsViewModel = ViewModelProviders.of(getActivity()).get(NewsViewModel.class);
        if(getArguments() != null)
            newsItem = new NewsItem(getString(R.string.test_pic), getString(R.string.test_title), getString(R.string.test_des), getString(R.string.test_fulldes));
        setpViewsAndData();
        addObserverOnViewModel();
    }

    private void addObserverOnViewModel() {
        newsViewModel.eventMessageLiveData.observe(this, new Observer<EventMessage>() {
            @Override
            public void onChanged(EventMessage event) {
                if (event.getType() == EventType.NEWS_API_FAILURE) {
                    Toast.makeText(getContext(), R.string.api_fail_string, Toast.LENGTH_LONG).show();
                    tvSync.setClickable(true);
                    tvSync.setEnabled(true);

                } else if (event.getType() == EventType.NEWS_API_DONE && event.getNewsItem() != null) {
                    NewsFragment.this.newsItem = event.getNewsItem();
                    setpViewsAndData();
                    tvSync.setClickable(true);
                    tvSync.setEnabled(true);
                }
            }
        });
    }

    private void setpViewsAndData() {
        tvSync = view.findViewById(R.id.tvSync);
        ((TextView) view.findViewById(R.id.tvTitle)).setText(newsItem.getTitle());
        ((TextView) view.findViewById(R.id.tvDes)).setText(newsItem.getFullDescription());
        Picasso.get().load(newsItem.getImageUrl()).placeholder(R.color.white).into((ImageView) view.findViewById(R.id.ivNews));
        view.findViewById(R.id.ivShare).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareBody = newsItem.getTitle() + getResources().getString(R.string.header_to_body_space) + newsItem.getFullDescription();
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.share_string));
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_using)));
            }
        });
        (view.findViewById(R.id.ivBack)).setOnClickListener(this);
        tvSync.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivBack:
                getActivity().onBackPressed();
                break;
            case R.id.tvSync:
                tvSync.setClickable(false);
                tvSync.setEnabled(false);
                Repository.getInstance().getNewsItem(newsItem.getId());
                break;
        }
    }
}
