package com.example.telecomnews.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.telecomnews.R;
import com.example.telecomnews.callback.ItemClickListener;
import com.example.telecomnews.model.NewsItem;
import com.squareup.picasso.Picasso;

public class NewsAdapter extends PagedListAdapter<NewsItem, NewsAdapter.ViewHolder> {

    private ItemClickListener itemClickListener;

    public NewsAdapter(ItemClickListener itemClickListener) {
        super(new DiffUtil.ItemCallback<NewsItem>() {
            @Override
            public boolean areItemsTheSame(@NonNull NewsItem oldItem, @NonNull NewsItem newItem) {
                return oldItem.getId() == newItem.getId();
            }

            @Override
            public boolean areContentsTheSame(@NonNull NewsItem oldItem, @NonNull NewsItem newItem) {
                return oldItem.equals(newItem);
            }
        });
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NewsItem news = getItem(position);
        if (news == null)
            return;
        ViewHolder holder1 = (ViewHolder) holder;
        holder1.tvTitle.setText(news.getTitle());
        holder1.tvDes.setText(news.getShortDescription());
        Picasso.get().load(news.getImageUrl()).placeholder(R.color.white).into(holder1.ivNews);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle;
        private TextView tvDes;
        private ImageView ivNews;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvDes = itemView.findViewById(R.id.tvDes);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            ivNews = itemView.findViewById(R.id.ivNews);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.itemClicked(getAdapterPosition());
                }
            });
        }
    }
}
