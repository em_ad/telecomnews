package com.example.telecomnews.model;

public class EventMessage {

    private EventType type;
    private NewsItem newsItem;

    public EventMessage(EventType type) {
        this.newsItem = null;
        this.type = type;
    }

    public EventMessage(EventType type, NewsItem newsItem) {
        this.type = type;
        this.newsItem = newsItem;
    }

    public EventType getType() {
        return type;
    }

    public NewsItem getNewsItem() {
        return newsItem;
    }
}
