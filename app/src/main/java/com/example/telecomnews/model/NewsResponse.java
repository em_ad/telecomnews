package com.example.telecomnews.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class NewsResponse implements Serializable {

    @SerializedName("total")
    int count;
    @SerializedName("data")
    List<NewsItem> newsItems;

    public int getCount() {
        return count;
    }

    public List<NewsItem> getNewsItems() {
        return newsItems;
    }
}
