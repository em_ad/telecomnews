package com.example.telecomnews.model;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


@Entity(tableName = "news_db")
public class NewsItem implements Serializable {

    @SerializedName("id")
    @PrimaryKey
    private int id;
    @SerializedName("title")
    @ColumnInfo(name = "title")
    private String title;
    @SerializedName("shortDescription")
    @ColumnInfo(name = "shortDescription")
    private String shortDescription; //short description
    @SerializedName("fullDescription")
    @ColumnInfo(name = "fullDescription")
    private String fullDescription; //long description
    @SerializedName("imageUrl")
    @ColumnInfo(name = "imageUrl")
    private String imageUrl;
    @SerializedName("isDeleted")
    @ColumnInfo(name = "isDeleted")
    private Boolean deleted;
    @SerializedName("creationTime")
    @ColumnInfo(name = "creationTime")
    private Long creationTime;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) return false;
        NewsItem fresh = (NewsItem) obj;
        return nullChecker(fresh.getTitle(), String.class).equals(title) &&
                nullChecker(fresh.getFullDescription(), String.class).equals(fullDescription) &&
                nullChecker(fresh.getShortDescription(), String.class).equals(shortDescription) &&
                nullChecker(fresh.getImageUrl(), String.class).equals(imageUrl) &&
                nullChecker(fresh.getCreationTime(), Long.class).equals(creationTime) &&
                nullChecker(fresh.isDeleted(), Boolean.class).equals(deleted);
    }

    private Object nullChecker(Object o, Class<?> cls) {
        if (o == null)
            return new Object();
        else return cls.cast(o);
    }

    public NewsItem(int id, String title, String shortDescription, String fullDescription) {
        this.title = title;
        this.shortDescription = shortDescription;
        this.fullDescription = fullDescription;
    }

    public NewsItem(String pic, String title, String shortDescription, String fullDescription) {
        this.id = 0;
        this.title = title;
        this.shortDescription = shortDescription;
        this.fullDescription = fullDescription;
        this.imageUrl = pic;
    }
}
