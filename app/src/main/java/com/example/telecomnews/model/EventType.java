package com.example.telecomnews.model;

public enum EventType {

    NEWS_API_FAILURE,
    NEWS_API_DONE,
    API_OVER,
    API_LOADING

}
