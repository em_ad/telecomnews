package com.example.telecomnews.vm;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.example.telecomnews.ui.adapter.NewsAdapter;
import com.example.telecomnews.model.EventMessage;
import com.example.telecomnews.model.EventType;
import com.example.telecomnews.model.NewsItem;
import com.example.telecomnews.util.Constants;
import com.example.telecomnews.util.Repository;

public class NewsViewModel extends ViewModel {

    public LiveData<PagedList<NewsItem>> liveData;
    public MutableLiveData<EventMessage> eventMessageLiveData;
    private int page = 0;
    private boolean paginating = false;

    public NewsViewModel() {
        eventMessageLiveData = new MutableLiveData<>();
        setObserverOnRepo();
        setupLiveData();
    }

    private void setObserverOnRepo() {
        Repository.getInstance().exposeCommunicator().observeForever(new Observer<EventMessage>() {
            @Override
            public void onChanged(EventMessage eventMessage) {
                eventMessageLiveData.postValue(eventMessage);
            }
        });
    }

    public void checkResults(PagedList<NewsItem> newsItems, NewsAdapter adapter) {
        if (newsItems.isEmpty()) {
            return;
        }
        adapter.submitList(newsItems);
    }

    public void refreshFeed() {
        page = 0;
        setupLiveData();
    }

    public void paginate(int newPage) {
        Repository.getInstance().getNews(newPage);
    }

    private void setupLiveData() {
        liveData = new LivePagedListBuilder<>(Repository.getInstance().getNews(), Constants.PAGINATION_SIZE).build();
    }

    public void checkForPagination(int lastVisibleItemPosition) {
        if (lastVisibleItemPosition > page * Constants.PAGINATION_SIZE - Constants.EARLY_PAGINATION_ACCESS_SIZE && !paginating) {
            paginate(++page);
            paginating = true;
            eventMessageLiveData.postValue(new EventMessage(EventType.API_LOADING));
        }
    }

    public void apiDone() {
        paginating = false;
    }
}
