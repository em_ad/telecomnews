package com.example.telecomnews.util;

public class Constants {

   public static final String BASE_URL = "https://api.telecomapp.ir/";
   public static final String ROOM_NAME = "room_news";
   public static final String NEWS_FRAGMENT_BACKSTACK_NAME = null;

   public static final int EARLY_PAGINATION_ACCESS_SIZE = 2;
   public static final int PAGINATION_SIZE = 7;
   public static final int ROOM_DB_VERSION = 1;
}
