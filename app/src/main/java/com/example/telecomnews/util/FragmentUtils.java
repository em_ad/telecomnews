package com.example.telecomnews.util;

import androidx.fragment.app.Fragment;

import com.example.telecomnews.ui.MainActivity;
import com.example.telecomnews.ui.fragment.NewsFragment;

public class FragmentUtils {

    public static void addFragment(MainActivity activity, int resId, Fragment fragment){

        activity.getSupportFragmentManager()
                .beginTransaction()
                .add(resId, fragment)
                .addToBackStack(NewsFragment.class.getSimpleName())
                .commitAllowingStateLoss();

    }

}
