package com.example.telecomnews.util;

import androidx.room.Room;

import com.example.telecomnews.App;
import com.example.telecomnews.callback.DBinterface;

public class DBaccess {

    private static DBaccess dBaccess = null;

    private DBinterface db;

    public DBaccess() {
        db = Room.databaseBuilder(App.getContext(), DBinterface.class, Constants.ROOM_NAME).allowMainThreadQueries().build();
        dBaccess = this;
    }

    public static DBaccess getInstance(){
        if(dBaccess == null)
            dBaccess = new DBaccess();
        return dBaccess;
    }

    public DBinterface getDb(){
        return db;
    }
}
