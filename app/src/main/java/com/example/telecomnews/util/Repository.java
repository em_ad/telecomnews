package com.example.telecomnews.util;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;

import com.example.telecomnews.callback.NewsAPI;
import com.example.telecomnews.model.EventMessage;
import com.example.telecomnews.model.EventType;
import com.example.telecomnews.model.NewsItem;
import com.example.telecomnews.model.NewsResponse;
import com.example.telecomnews.vm.NewsViewModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository {

    private static Repository instance;

    public boolean testing = false;

    private MutableLiveData<EventMessage> communicationLiveData = new MutableLiveData<>();

    public static Repository getInstance(){
        if(instance == null)
            instance = new Repository();
        return instance;
    }

    public MutableLiveData<EventMessage> exposeCommunicator(){
        return communicationLiveData;
    }

    public DataSource.Factory<Integer, NewsItem> getNews() {
        initiateAPIFetch(0);
        return DBaccess.getInstance().getDb().newsDao().getAll();
    }

    public void getNews(int newPage) {
        initiateAPIFetch(newPage);
    }

    private synchronized void initiateAPIFetch(int page) {
        if(testing)
            communicationLiveData.postValue(new EventMessage(EventType.API_LOADING));
        Call<NewsResponse> call = ServiceGenerator.createService(NewsAPI.class).fetchNews(page);
        Callback<NewsResponse> responseCallback = new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        if (response.body().getNewsItems().size() == 0) {
//                            viewModel.eventMessageLiveData.postValue(new EventMessage(EventType.API_OVER));
                            communicationLiveData.postValue(new EventMessage(EventType.API_OVER));
                        } else {
                            DBaccess.getInstance().getDb().newsDao().insertAll(response.body().getNewsItems());
//                            viewModel.eventMessageLiveData.postValue(new EventMessage(EventType.NEWS_API_DONE));
                            communicationLiveData.postValue(new EventMessage(EventType.NEWS_API_DONE));
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
//                viewModel.eventMessageLiveData.postValue(new EventMessage(EventType.NEWS_API_FAILURE));
                communicationLiveData.postValue(new EventMessage(EventType.NEWS_API_FAILURE));
            }
        };
        call.enqueue(responseCallback);
    }

    public void getNewsItem(int id) {
        Call<NewsItem> call = ServiceGenerator.createService(NewsAPI.class).fetchSingleNews(id);
        call.enqueue(new Callback<NewsItem>() {
            @Override
            public void onResponse(Call<NewsItem> call, Response<NewsItem> response) {
                if(response.body() == null){
                    onFailure(call, new Throwable());
                    return;
                }
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        DBaccess.getInstance().getDb().newsDao().insert(response.body());
//                        viewModel.eventMessageLiveData.postValue(new EventMessage(EventType.NEWS_API_DONE, response.body()));
                        communicationLiveData.postValue(new EventMessage(EventType.NEWS_API_DONE, response.body()));
                    }
                });
            }

            @Override
            public void onFailure(Call<NewsItem> call, Throwable t) {
//                viewModel.eventMessageLiveData.postValue(new EventMessage(EventType.NEWS_API_FAILURE));
                communicationLiveData.postValue(new EventMessage(EventType.NEWS_API_FAILURE));
            }
        });
    }


}
