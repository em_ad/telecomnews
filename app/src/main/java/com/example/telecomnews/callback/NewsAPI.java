package com.example.telecomnews.callback;

import com.example.telecomnews.model.NewsItem;
import com.example.telecomnews.model.NewsResponse;
import com.example.telecomnews.util.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface NewsAPI {

    @GET("news/{page}/" + Constants.PAGINATION_SIZE)
    Call<NewsResponse> fetchNews(@Path("page") int page);

    @GET("news/{id}")
    Call<NewsItem> fetchSingleNews(@Path("id") int page);

}
