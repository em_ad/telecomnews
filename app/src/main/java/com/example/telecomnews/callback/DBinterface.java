package com.example.telecomnews.callback;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.telecomnews.model.NewsItem;
import com.example.telecomnews.util.Constants;

@Database(entities = {NewsItem.class}, version = Constants.ROOM_DB_VERSION, exportSchema = false)
public abstract class DBinterface extends RoomDatabase {
    public abstract NewsDao newsDao();
}
