package com.example.telecomnews.callback;

public interface ItemClickListener {
    void itemClicked(int position);
}
