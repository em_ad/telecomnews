package com.example.telecomnews.callback;

import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.telecomnews.model.NewsItem;

import java.util.List;


@Dao
public interface NewsDao {

    @Query("SELECT * FROM news_db ORDER BY creationTime DESC")
    DataSource.Factory<Integer, NewsItem> getAll();

    @Query("SELECT * FROM news_db")
    List<NewsItem> getAllSimple();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<NewsItem> newsItems);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(NewsItem newsItem);

    @Query("SELECT * FROM news_db WHERE id=:id ")
    NewsItem loadSingle(int id);

    @Delete
    void delete(NewsItem item);
}
