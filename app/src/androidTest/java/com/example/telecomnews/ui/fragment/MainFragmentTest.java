package com.example.telecomnews.ui.fragment;

import android.os.Bundle;
import android.os.SystemClock;

import androidx.fragment.app.testing.FragmentScenario;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.example.telecomnews.R;
import com.example.telecomnews.model.NewsItem;
import com.example.telecomnews.ui.MainActivity;
import com.example.telecomnews.util.DBaccess;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.*;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class MainFragmentTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    MainActivity activity;

    @Before
    public void setUp() throws Exception {
        activity = activityTestRule.getActivity();
    }

    @After
    public void tearDown() throws Exception {
        activity.finish();
        activity = null;
    }

    @Test
    public void checkClick(){
        onView(withId(R.id.flContainer))
                .check(matches(isDisplayed()));
        FragmentScenario.launchInContainer(NewsFragment.class, new Bundle());
        onView(withId(R.id.tvTitle)).check(matches(withText(R.string.test_title)));
        onView(withId(R.id.tvDes)).check(matches(withText(R.string.test_fulldes)));
        onView(withId(R.id.ivBack)).perform(click());
        assertNull(activity.findViewById(R.id.ivShare));
    }

    @Test
    public void checkList(){
        assertNull(((LinearLayoutManager)((RecyclerView)activity.findViewById(R.id.recycler)).getLayoutManager()).findViewByPosition(((RecyclerView)activity.findViewById(R.id.recycler)).getAdapter().getItemCount()-1));
        onView(withId(R.id.flContainer))
                .check(matches(isDisplayed()));
        onView(withId(R.id.recycler)).perform(swipeUp());
        SystemClock.sleep(2000);
        onView(withId(R.id.recycler)).perform(swipeUp());
        SystemClock.sleep(1000);
        assertNotNull(activity.findViewById(R.id.recycler));
        assertTrue(((RecyclerView)activity.findViewById(R.id.recycler)).getAdapter().getItemCount() == DBaccess.getInstance().getDb().newsDao().getAllSimple().size());
        assertNotNull(((LinearLayoutManager)((RecyclerView)activity.findViewById(R.id.recycler)).getLayoutManager()).findViewByPosition(((RecyclerView)activity.findViewById(R.id.recycler)).getAdapter().getItemCount()-1).findViewById(R.id.tvTitle));
    }
}